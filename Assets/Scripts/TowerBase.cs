﻿namespace AFSInterview
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    

    public abstract class TowerBase : MonoBehaviour
    {
        protected IReadOnlyList<Enemy> enemies;

        public abstract void Initialize(IReadOnlyList<Enemy> enemies);
    }
}