﻿namespace AFSInterview
{
    using UnityEngine;

    public class Cannonball : MonoBehaviour
    {
        [SerializeField]
        private float speed;
        [SerializeField]
        private float minArcHeight;
        [SerializeField]
        private float maxArcHeight;
        [SerializeField]
        private float maxArcDistanceFactor;

        private Vector3 startPosition;
        private Vector3 endPosition;

        private float startY;
        private float endY;
        private float distance;
        private float explosionRange = 0.7f;
        private float arcHeight;



        public void Initialize(Vector3 endPosition)
        {
            this.startPosition = transform.position;
            this.startY = startPosition.y;
            this.startPosition.y = 0;
            this.endPosition = endPosition;
            this.endY = endPosition.y;
            this.endPosition.y = 0;
            this.distance = (startPosition - endPosition).magnitude;

            this.arcHeight = Mathf.Lerp(minArcHeight, maxArcHeight, Mathf.Max(1, distance / maxArcDistanceFactor));
        }

        private void Update()
        {
            var direction = (endPosition - startPosition).normalized;

            Vector3 movementV = direction * speed * Time.deltaTime;
            Vector3 nextPos = transform.position + movementV;
            nextPos.y = 0;

            float travelledPathFactor = (nextPos - startPosition).magnitude / distance;
            float baseY = Mathf.Lerp(startY, endY, travelledPathFactor);
            float arc = Mathf.Sin(travelledPathFactor * Mathf.PI) * arcHeight;
            
            transform.position = nextPos + new Vector3(0, baseY + arc, 0);
                
            if ((nextPos - startPosition).magnitude > distance)
            {
                DestroyAllHitEnemies();
                Destroy(gameObject);
            }
        }
        private void DestroyAllHitEnemies()
        {
            foreach (Collider c in Physics.OverlapSphere(transform.position, explosionRange))
            {
                if (c.GetComponent<Enemy>())
                {
                    Destroy(c.gameObject);
                }
            }
        }

        public float GetSpeed()
        {
            return speed;
        }
        public void SetSpeed(float newSpeed)
        {
            this.speed = newSpeed;
        }
    }
}