﻿namespace AFSInterview
{
    using System.Collections;
    using System.Collections.Generic;
    using TMPro;
    using UnityEngine;

    public class UIController : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI scoreText;
        [SerializeField]
        private TextMeshProUGUI enemiesCountText;

        public void UpdateGUI(float score, float enemies)
        {
            scoreText.text = "Score: " + score;
            enemiesCountText.text = "Enemies: " + enemies;
        }

    }

}
