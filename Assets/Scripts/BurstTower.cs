﻿namespace AFSInterview
{
    using System.Collections.Generic;
    using UnityEngine;

    public class BurstTower : TowerBase
    {
        [SerializeField] private Cannonball cannonballPrefab;
        [SerializeField] private Transform bulletSpawnPoint;
        [SerializeField] private Transform cannonAnchor;
        [SerializeField] private float firingRate;
        [SerializeField] private float burstFireRate;
        [SerializeField] private float firingRange;
        [SerializeField] private int burstCannonballCount = 3;

        [SerializeField] private float maxCannonAngle = 60;


        private float fireTimer;
        private Enemy targetEnemy;

        private bool currentlyShooting = false;

        private Vector3 nextEnemyPos = Vector3.zero;

        public override void Initialize(IReadOnlyList<Enemy> enemies)
        {
            this.enemies = enemies;
            fireTimer = firingRate;
        }

        private void Update()
        {
            if(!currentlyShooting)
            {
                targetEnemy = FindClosestEnemy();
            }
            if (targetEnemy != null)
            {
                nextEnemyPos = targetEnemy.gameObject.transform.position + targetEnemy.GetNormalizedDirection();

                var lookRotation = Quaternion.LookRotation(nextEnemyPos - transform.position);
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, lookRotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                cannonAnchor.localRotation = Quaternion.Euler(-Mathf.Lerp(0, maxCannonAngle, (nextEnemyPos-transform.position).magnitude/firingRange), 0, 0);
            }

            fireTimer -= Time.deltaTime;
            if (fireTimer <= 0f)
            {
                StartCoroutine(ShootingCoroutine());
            }
        }

        System.Collections.IEnumerator ShootingCoroutine()
        {
            currentlyShooting = true;
            fireTimer = firingRate;
            for(int i = 0; i<burstCannonballCount; i++)
            {
                if (targetEnemy != null)
                {
                    Cannonball cannonball = Instantiate(cannonballPrefab, bulletSpawnPoint.position, Quaternion.identity);
                    cannonball.Initialize(nextEnemyPos);
                    cannonball.SetSpeed((transform.position - nextEnemyPos).magnitude);
                    yield return new WaitForSeconds(burstFireRate);
                }
            }
            currentlyShooting = false;
        }

        private Enemy FindClosestEnemy()
        {
            Enemy closestEnemy = null;
            var closestDistance = float.MaxValue;

            foreach (var enemy in enemies)
            {
                var distance = (enemy.transform.position - transform.position).magnitude;
                if (distance <= firingRange && distance <= closestDistance)
                {
                    closestEnemy = enemy;
                    closestDistance = distance;
                }
            }

            return closestEnemy;
        }
    }
}
