﻿namespace AFSInterview
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class SerializableTower
    {
        public TOWER_TYPE type;
        public TowerBase tower;
    }
}