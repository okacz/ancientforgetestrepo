﻿namespace AFSInterview
{
    using UnityEngine;

    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float speed;

        private GameObject targetObject;
        private Vector3 lastTargetPos;

        public void Initialize(GameObject target)
        {
            targetObject = target;
        }

        private void Update()
        {
            Vector3 target;
            if(!targetObject)
            {
                target = lastTargetPos;
            }
            else
            {
                target = targetObject.transform.position;
                lastTargetPos = target;
            }


            var direction = (target - transform.position).normalized;

            transform.position += direction * speed * Time.deltaTime;

            if ((transform.position - target).magnitude <= 0.2f)
            {
                Destroy(gameObject);
                Destroy(targetObject);
            }
        }
    }
}