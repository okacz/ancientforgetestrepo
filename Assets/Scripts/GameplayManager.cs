﻿namespace AFSInterview
{
    using System.Collections.Generic;
    using TMPro;
    using UnityEngine;


    public class GameplayManager : MonoBehaviour
    {

        [Header("Prefabs")] 
        [SerializeField] private GameObject enemyPrefab;

        [SerializeField] private List<SerializableTower> towerList;

        [Header("Settings")] 
        [SerializeField] private Vector2 boundsMin;
        [SerializeField] private Vector2 boundsMax;
        [SerializeField] private float enemySpawnRate;

        [Header("UI")]
        [SerializeField] private UIController uiController;

        private Dictionary<TOWER_TYPE, TowerBase> towerDictionary = new Dictionary<TOWER_TYPE, TowerBase>();
        private List<Enemy> enemies;
        private float enemySpawnTimer;
        private int score;

        private void Awake()
        {
            enemies = new List<Enemy>();
            CreateTowerDictionary();
        }

        private void Update()
        {
            enemySpawnTimer -= Time.deltaTime;

            if (enemySpawnTimer <= 0f)
            {
                SpawnEnemy();
                enemySpawnTimer = enemySpawnRate;
            }

            MouseInput();
            uiController.UpdateGUI(score, enemies.Count);
        }
        private void CreateTowerDictionary()
        {
            foreach(SerializableTower st in towerList)
            {
                towerDictionary.Add(st.type, st.tower);
            }
        }
        private void MouseInput()
        {
            TOWER_TYPE type = TOWER_TYPE.TOWER_TYPE_BASE;
            if (Input.GetMouseButtonDown(0))
            {
                type = TOWER_TYPE.TOWER_TYPE_BASE;
            }
            else
            if(Input.GetMouseButtonDown(1))
            {
                type = TOWER_TYPE.TOWER_TYPE_BURST;
            }
            else
            {
                return;
            }

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out var hit, LayerMask.GetMask("Ground")))
            {
                var spawnPosition = hit.point;
                SpawnTower(spawnPosition, type);
            }
        }
        private void SpawnEnemy()
        {
            var position = new Vector3(Random.Range(boundsMin.x, boundsMax.x), enemyPrefab.transform.position.y, Random.Range(boundsMin.y, boundsMax.y));
            
            var enemy = Instantiate(enemyPrefab, position, Quaternion.identity).GetComponent<Enemy>();
            enemy.OnEnemyDied += Enemy_OnEnemyDied;
            enemy.Initialize(boundsMin, boundsMax);

            enemies.Add(enemy);
        }

        private void Enemy_OnEnemyDied(Enemy enemy)
        {
            enemies.Remove(enemy);
            score++;
        }

        private void SpawnTower(Vector3 position, TOWER_TYPE type)
        {
            TowerBase tb = towerDictionary[type];

            position.y = tb.transform.position.y;
            var tower = Instantiate(tb, position, Quaternion.identity);
            tower.Initialize(enemies);
        }
    }
}